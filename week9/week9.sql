select count(Customers.CustomerID) as numberOfCustomers, Customers.Country
from Customers
group by Customers.Country
order by count(Customers.CustomerID) desc;

select Products.ProductID, Suppliers.SupplierName
from Products join Suppliers on Products.SupplierID = Suppliers.SupplierID
group by Products.SupplierID
order by count(Products.ProductID) desc;

load data local infile "/home/a.turan14/Downloads/Untitled Folder/customers.csv" 
into table Customers 
fields terminated by ';' 
ignore 1 lines;